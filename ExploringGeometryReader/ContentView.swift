//
//  ContentView.swift
//  ExploringGeometryReader
//
//  Created by Jeffry Sandy Purnomo on 01/04/21.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        NavigationView{
//            GeometryReader{ proxy in
                ScrollView(.horizontal){
                    HStack{
                        ForEach(0...20, id: \.self){ num in
                            GeometryReader{ proxy in
                                let x = proxy.frame(in: .global).origin.x
                                
                                Text("\(Int(x))")
                                    .bold()
                                    .font(.system(size: 24))
                                    .frame(width: 100, height: 100, alignment: .center)
                            }
                            .frame(width: 100, height: 100, alignment: .center)
                            .background(Color.blue)
                            .padding()
                        }
                    }
                }
//                let frame = proxy.frame(in : .global)
//                let frame = proxy.frame(in : .local)
//
//                HStack{
//                    Text(frame.debugDescription)
//                        .frame(width: proxy.size.width/2, height: proxy.size.height/2, alignment: .center)
//                        .background(Color.blue)
//                    Text("Second")
//                        .frame(width: proxy.size.width/2, height: proxy.size.height/2, alignment: .center)
//                        .background(Color.red)
//                }
//            }
            .background(Color.purple)
            .navigationTitle("Geometry Reader")
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
