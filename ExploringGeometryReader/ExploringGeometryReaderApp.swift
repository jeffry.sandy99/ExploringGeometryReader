//
//  ExploringGeometryReaderApp.swift
//  ExploringGeometryReader
//
//  Created by Jeffry Sandy Purnomo on 01/04/21.
//

import SwiftUI

@main
struct ExploringGeometryReaderApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
